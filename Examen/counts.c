#include<stdio.h>
#include<stdlib.h>

int count_char(char *string, char c){//Fonction qui compte un nombre de caractere precis
	char *p;
	int cpt = 0;
	p = string;
	while(*p){
		if(*p == c){
			cpt += 1;
		}
        *p++;
	}
	return cpt;
}
int count_words(char *string){ // Fonction qui compte le nombre de mot par espace
    char *p;
    p = string;  
    int cpt = 0;  
    while(*p++){
        if(*p == ' '){
            cpt += 1;
        }
    }
    cpt += 1;
    return cpt;
}
int count_words_better(char *string){//Amelioration de la fonction au dessus
    char *p;
    p = string;  
    int cpt = 0;  
    while(*p++){
        if(*p == ' '){//Lorsque le pointeur detecte un espase il rentre dans la bouble
while(*p == ' '){//Tant que le pointeur pointe un espace il avance jusqu'à ce qu'ilrencontre autre chose 
                *p++;            
            }
            cpt++;
        }
    }
    cpt += 1;//Retourne le pointeur 
    return cpt;
}
int main(){
	char word[50];
	printf("Votre mot: ");
	scanf("%s[^\n]", &word[50]);
	int char_count = count_char(word, 'i');
	printf("%d\n", char_count);
    int words_count = count_words(word);
    printf("%d", words_count);
    int better_words_count = count_words_better(word);
    printf("%d", better_words_count);
}
