#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void add_vectors(int n, double *t1, double *t2, long double *t3){
    long double difference = 0;
    for(int i = 0;i < n; i++){
            difference = (*t1/(*t2))*100;
            *t3 = difference;
            printf("%Lf ", *t3);
            *t1++;
            *t2++;
            *t3++;
    }
    printf("\n");
}
double norm_vector(int n, double *t){
    double euclidienne;
    for(int i = 0; i < n; i++){
        euclidienne += *t * (*t);
        *t++; 
    }
    return sqrtl(euclidienne);
}

int main() {
	double T1[] = { 3.14, -1.0, 2.3, 0, 7.1 };
	double T2[] = { 2.71,  2.5,  -1, 3, -7  };
	long double T3[5];
	int n = 5;
    add_vectors(5, T1, T2, T3);
    double vector_euclidienne = norm_vector(5, T1);
    printf("La norme euclidienne du vecteur est de: %lf\n", vector_euclidienne);

	printf("T1 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T1[i]);
	}
	printf("\nT2 : ");
	for (int i = 0; i < n; i++) {
		printf("%+.2lf ", T2[i]);
	}
	printf("\n");
	
	exit(EXIT_SUCCESS);
}
