#include <stdio.h>
#include <stdlib.h>

void small_to_zero(int *t, int n, int val)
{   
    int i = 0;
    int j = 0;
	while (i < n)
    {
        *t = i;
		while(j <= val)
        {
			*t = j;
			j++;
			if(j > val)
            {
				j = 0;
			}
            break;
		}
        printf("%d", *t);
        i++;
	}
}

int main(){
	int tab[] = { 1, 3, -1, 2, 7, 5, 12, 4, 4, 3, 0 };
	small_to_zero(tab, 11, 6);
	return 0;
}
